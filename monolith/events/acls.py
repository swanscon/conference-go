import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }  # dictionary for the headers to use in the request
    url = "https://api.pexels.com/v1/search"  # url for the request
    params = {
        "query": f"{city} {state}",
        "per_page": 1,
    }  # query for the url with city and state
    response = requests.get(
        url, params=params, headers=headers
    )  # make the request
    pexel_dict = response.json()  # parse the json response
    picture_url = pexel_dict["photos"][0]["src"][
        "original"
    ]  # url for one of the pictures in the response
    return {"picture_url": picture_url}  # return the dictionary


def get_weather(city, state):
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    geo_url = "http://api.openweathermap.org/geo/1.0/direct?q="
    geo_params = {"query": f"{city} {state}", "limit": 1}
    geo_response = requests.get(
        geo_url, geo_params=geo_params, headers=headers
    )
    geo_dict = geo_response.json()
    lat = geo_dict["lat"]
    lon = geo_dict["lon"]

    weather_url = "https://api.openweathermap.org/data/2.5/weather?"
    params = {"query": f"{lat} {lon}"}
    response = requests.get(weather_url, params=params, headers=headers)
    weather_dict = response.json()
    temp = weather_dict["main.temp"]
    weather = weather_dict["weather.description"]
    return temp, weather
